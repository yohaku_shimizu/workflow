<?php

$project_id = intval($vars['id']);
$project_items = ORM::for_table('project_item')
->where('project_id', $project_id)
->find_many();

if ($project_items) {
  $project_items = sortByKey('priority', SORT_ASC, $project_items);
}

?>
