<?php

function home_url() {
  global $home_url;
  return $home_url;
}

function temp_dir() {
  $home_url = home_url();
  $home_url .= "src/theme";

  return $home_url;
}

function get_user() {
  global $user_id;
  $user_data = ORM::for_table('users')->where('id', $user_id)->find_one();
  return $user_data;
}

function debug($data) {
  echo "<pre class=\"prettyprint\">";
  var_dump($data);
  echo "</pre>";
}

function get_id($user_id) {
  $return_number = str_pad($user_id, 6, 0, STR_PAD_LEFT);

  return $return_number;
}


?>
