<div class="client-add-form part">
  <h3>新規顧客アカウントを作成</h3>

  <div class="client-add-form__wrap">
    <form method="post" action="<?= home_url() . "yoadmin/clientadd/insert" ?>" class="template">

      <div class="form-parts">
        <label>顧客番号</label>
        <input name="number" type="number">
      </div>

      <div class="form-parts">
        <label>Login ID</label>
        <input name="id" type="text">
      </div>

      <div class="form-parts">
        <label>Login PASSWORD</label>
        <input name="password" type="text">
      </div>

      <div class="form-parts">
        <label>顧客名</label>
        <input name="client_name" type="text">
      </div>

      <div class="form-parts">
        <label>電話番号</label>
        <input name="tel" type="tel">
      </div>

      <div class="form-parts">
        <label>メールアドレス</label>
        <input name="mail" type="mail">
      </div>
      <p class="text-r"><button class="button">追加</button></p>
    </form>
  </div>
</div>
