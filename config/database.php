<?php
// DB設定
ORM::configure('mysql:host=localhost;dbname=workflow');
ORM::configure('username', 'root');
ORM::configure('password', '');
ORM::configure('driver_options', [
  PDO::MYSQL_ATTR_INIT_COMMAND       => 'SET NAMES utf8',
  PDO::ATTR_EMULATE_PREPARES         => false,
  PDO::MYSQL_ATTR_USE_BUFFERED_QUERY => true,
]);
?>
