
<?php
var_dump($_POST);

$res = null;
$options = array();

$password = $_POST['password'];


$res = password_hash( $password, PASSWORD_DEFAULT);

$client_number = intval($_POST['number']);

try {

  $error_flg = true;

  $record = ORM::for_table('users')->create();
  $record->client_number = $client_number;
  $record->login_id = $_POST['id'];
  $record->login_password = $res;
  $record->name = $_POST['client_name'];
  $record->tel = $_POST['tel'];
  $record->email = $_POST['mail'];
  $record->created = date('Y-m-d');

  $record->save();

} catch (\Exception $e) {
  $error_flg = false;
  echo "error";
} finally {
  if ($error_flg) {
    echo "成功";
  }
}


global $home_url;
header('Location:' . $home_url . 'yoadmin/client');

?>
