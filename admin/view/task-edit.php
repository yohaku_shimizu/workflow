<?php include 'header.php' ?>

<div class="admin-wrap">

  <div class="sidebar-wrap"><?php include 'sidebar.php' ?></div>

  <div class="content-wrap">
    <?php
    include 'parts/task-edit/form.php';
    ?>
  </div>

</div>
<?php include 'footer.php' ?>
