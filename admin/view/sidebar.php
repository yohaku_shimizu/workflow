<div class="sidebar">
  <h1>
    <a href="<?= home_url() ?>yoadmin/dashboard"><img src="<?= temp_admin() ?>/images/logo_ie.svg" />Admin</a>
  </h1>

  <div class="sidebar__list">
    <ul>
      <li class="active">
        <a href="<?= home_url() ?>yoadmin/client"><i class="far fa-user"></i> クライアント</a>
        <ul>
          <li><a href="<?= home_url() ?>yoadmin/clientadd">クライアント追加</a></li>
          <li><a href="<?= home_url() ?>yoadmin/client">クライアント一覧</a></li>
        </ul>
      </li>
      <li>
        <a href="<?= home_url() ?>yoadmin/projects"><i class="fas fa-project-diagram"></i> プロジェクト</a>
        <ul>
          <li><a href="<?= home_url() ?>yoadmin/projects">進行中プロジェクト一覧</a></li>
          <li><a href="<?= home_url() ?>yoadmin/projects/all">プロジェクト一覧</a></li>
          <li><a href="<?= home_url() ?>yoadmin/projectadd">プロジェクト追加</a></li>
        </ul>
      </li>
      <!-- <li>
        <a href="<?= home_url() ?>yoadmin"><i class="fas fa-clipboard-list"></i> 開発工程</a>
        <ul>
          <li><a href="<?= home_url() ?>yoadmin">各開発工程一覧</a></li>
          <li><a href="<?= home_url() ?>yoadmin">開発進捗</a></li>
        </ul>
      </li> -->
      <li>
        <a href="<?= home_url() ?>yoadmin"><i class="fas fa-tasks"></i> タスク</a>
        <ul>
          <li><a href="<?= home_url() ?>yoadmin">未承認タスク一覧</a></li>
          <li><a href="<?= home_url() ?>yoadmin/task/all">タスク一覧</a></li>
        </ul>
      </li>
      <li>
        <a href="##"><i class="fas fa-book"></i> 日報</a>
        <ul>
          <li><a>今日の日報を書く</a></li>
          <li>日報確認</li>
        </ul>
      </li>
    </ul>
  </div>
</div>
