<?php

$process_id = intval($vars['id']);
$project_item = ORM::for_table('project_item')
->where('id', $process_id)
->find_one();

$mytasks = ORM::for_table('my_task')
->where('project_item_id',$process_id)
->find_many();

if ($mytasks) {
  $mytasks = sortByKey('priority', SORT_ASC, $mytasks);
}

?>
