<?php
include 'method.php';
require 'config/database.php'; // DB情報 読み込み

include 'data_load.php';
?>
<!DOCTYPE html>
<html lang="ja">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width,initial-scale=1">
    <title></title>
    <link rel="stylesheet" href="<?= temp_dir() ?>/css/common.min.css" />

    <!-- 消さない -->
    <script src="https://cdn.rawgit.com/google/code-prettify/master/loader/run_prettify.js?lang=css&skin=sunburst"></script>

    <!-- WEB Font -->
    <script>
    (function(d) {
      var config = {
        kitId: 'dii4ntd',
        scriptTimeout: 3000,
        async: true
      },
      h=d.documentElement,t=setTimeout(function(){h.className=h.className.replace(/\bwf-loading\b/g,"")+" wf-inactive";},config.scriptTimeout),tk=d.createElement("script"),f=false,s=d.getElementsByTagName("script")[0],a;h.className+=" wf-loading";tk.src='https://use.typekit.net/'+config.kitId+'.js';tk.async=true;tk.onload=tk.onreadystatechange=function(){a=this.readyState;if(f||a&&a!="complete"&&a!="loaded")return;f=true;clearTimeout(t);try{Typekit.load(config)}catch(e){}};s.parentNode.insertBefore(tk,s)
      })(document);
    </script>

    <!-- jQuery読み込み  -->
    <script src="https://code.jquery.com/jquery-3.3.1.js"></script>

    <!-- ドロワーメニュー -->
    <script type="text/javascript">
    $(function() {
      $('.hamburger').click(function() {
          $(this).toggleClass('active');

          if ($(this).hasClass('active')) {
              $('.globalMenuSp').addClass('active');
          } else {
              $('.globalMenuSp').removeClass('active');
          }
      });
    });
    </script>

    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.9.0/css/all.min.css" integrity="sha256-UzFD2WYH2U1dQpKDjjZK72VtPeWP50NoJjd26rnAdUI=" crossorigin="anonymous" />
  </head>
  <body>

<header class="header">

  <div class="header__contents flex">
    <p class="header__contents--client-name"><?= $user["name"] . "さん";?></p>
    <p class="header__contents--client-num"><?= "No." . get_id($user["id"]);?></p>

      <p class="header__contents--client-info pc"><a href="#"><i class="far fa-user-circle"></i>アカウント情報</a></p>
      <p class="header__contents--client-guide pc"><a href="#"><i class="far fa-question-circle"></i>ご利用ガイド</a></p>
      <p class="header__contents--client-contact pc"><a href="#"><i class="fas fa-envelope"></i>お問い合わせ</a></p>
      <p class="header__contents--client-logout pc"><a href="<?= home_url() ?>logout"><i class="fas fa-sign-out-alt"></i>ログアウト</a></p>

    <div class="hamburger sp">
      <span></span>
      <span></span>
      <span></span>
    </div>

    <nav class="globalMenuSp sp">
      <ul>
        <li><a href="#"><i class="far fa-user-circle"></i>アカウント情報</a></li>
        <li><a href="#"><i class="far fa-question-circle"></i>ご利用ガイド</a></li>
        <li><a href="#"><i class="fas fa-envelope"></i>お問い合わせ</a></li>
        <li><a href="<?= home_url() ?>logout"><i class="fas fa-sign-out-alt"></i>ログアウト</a></li>
      </ul>
    </nav>

  </div>

</header>
