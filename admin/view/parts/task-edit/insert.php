<?php
//var_dump($_POST);
$id = intval($_POST['project_item_id']);
$upload_type = intval($_POST['upload_type']);

$record = ORM::for_table('client_task')->where('id', $id)->find_one();
$record->name = $_POST['name'];
$record->upload_type = $upload_type;
$record->memo = $_POST['text'];
$record->save();

global $home_url;
header('Location:' . $home_url . 'yoadmin/task/all');
?>
