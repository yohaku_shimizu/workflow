
<?php include 'db.php' ?>

<!-- include libraries(jQuery, bootstrap) -->

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>
<script src="https://stackpath.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>

<!-- include summernote css/js -->

<script src="https://cdn.jsdelivr.net/npm/summernote@0.8.18/dist/summernote.min.js"></script>

<div class="task-add part">
  <h3>タスクの編集</h3>
  <form method="post" enctype="multipart/form-data" action="<?= home_url() . "yoadmin/task/edit/insert" ?>" class="template">

    <input type="hidden" name="project_item_id" value="<?= $vars['id'] ?>">

    <div class="name form-parts">
      <label>タスクタイトル</label>
      <input name="name" type="text" value="<?= $task->name ?>">
    </div>

    <div class="type">
      <label>提出形式</label>
      <div class="radio-wrap">
        <input type="radio" name="upload_type" value="1" <?php if ($task->upload_type == 1): ?>checked<?php endif; ?>>テキスト
        <input type="radio" name="upload_type" value="2" <?php if ($task->upload_type == 2): ?>checked<?php endif; ?>>ファイル
        <input type="radio" name="upload_type" value="3" <?php if ($task->upload_type == 3): ?>checked<?php endif; ?>>承認
      </div>
    </div>

    <div class="text">
      <label>タスクの指示</label>
      <!-- <textarea id="summernote" name="editordata" ></textarea> -->
      <textarea name="text" id="summernote"><?= $task->memo ?></textarea>
    </div>

    <p class="text-r">
      <input class="button" type="submit" name="send" value="変更を完了する">
    </p>

  </form>
</div>


<script>

var url_origin = location.origin;
var img_up_file = url_origin + "/images/upload.php";
console.log(img_up_file);


$(document).ready(function() {
  var temp_admin = JSON.parse('<?php echo $java_temp_admin; ?>');
  var upload_file = temp_admin + "/summernote/upimg.php";
  var upload_directory = temp_admin + "/images/taskImages/";
  $('#summernote').summernote({
    height: 600,
    fontNames: ["YuGothic","Yu Gothic","Hiragino Kaku Gothic Pro","Meiryo","sans-serif", "Arial","Arial Black","Comic Sans MS","Courier New","Helvetica Neue","Helvetica","Impact","Lucida Grande","Tahoma","Times New Roman","Verdana"],
    lang: "ja-JP",
    callbacks: {
      onImageUpload: function(files, editor, welEditable) {
      var url= upimage(files[0], editor, welEditable);
    }
    }
  });

  function upimage(file,editor,welEditable) {

    const formData = new FormData();

    formData.append("avatar", file);  // ファイル内容を詰める

    const param = {
      method: "POST",
      body: formData
    }

    // アップロードする
    fetch(img_up_file, param)
    .then((res)=>{
      return( res.json() );
    })
    .then((json)=>{
      // 通信が成功した際の処理
      console.log(json);
      var url = json['status'];
      let html = $('#summernote').summernote('code');
      $('#summernote').summernote('code', html + '<img src="' + url + '"/>');
    })
    .catch((error)=>{
      // エラー処理
    });

  }

});

</script>
