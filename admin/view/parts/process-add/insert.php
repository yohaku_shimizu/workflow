
<?php
//var_dump($_POST);

$project_id = $_POST['project_id'];

$set_count = 0;
$ary_target = 0;
$process_ary = array();
foreach ($_POST['list'] as $index => $value) {
  $key = array_keys($value);
  $process_ary[$ary_target][$key[0]] = $value[$key[0]];
  $set_count++;
  if ($set_count == 3) {
    $ary_target++;
    $set_count = 0;
  }
}
//var_dump($project_id);
//var_dump($process_ary);

foreach ($process_ary as $index => $process) {

  if ($process['id'] == "new") {
    $record = ORM::for_table('project_item')->create();
    $record->name = $process['name'];
    $record->project_id = $project_id;
    $record->priority = $index;
    $record->memo = $process['memo'];
    $record->created = date('Y-m-d');
    $record->save();
  }else {
    $id = intval($process['id']);
    $record = ORM::for_table('project_item')->where('id',$id)->find_one();
    $record->name = $process['name'];
    $record->project_id = $project_id;
    $record->priority = $index;
    $record->memo = $process['memo'];
    $record->save();
  }
}

global $home_url;
header('Location:' . $home_url . 'yoadmin/process/view/' . $project_id);
?>
