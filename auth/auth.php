<?php
session_start();
if (isset($_SESSION['id'])) {
  $user_id = $_SESSION['id'];
  $user_name = $_SESSION['name'];
}else {
  // header('Location: ' . $_SERVER['HTTP_REFERER']);
  // exit;
}

if (isset($_POST['login_id'])) {

  $login_id = $_POST['login_id'];
  $login_pass = $_POST['login_pass'];

  $auth_user = ORM::for_table('users')->where('login_id', $login_id)->find_one();

  if ($auth_user) {

    if (password_verify($login_pass, $auth_user->login_password)) {
      $_SESSION['name'] = $auth_user->name;
      $_SESSION['id'] = $auth_user->id;
      $_SESSION['admin'] = $auth_user->admin;
    }else {
      header('Location:' . $home_url . 'auth/login.php?status="notpass"');
    }

  }else {
    header('Location:' . $home_url . 'auth/login.php?status="notid"');
  }

}

if (!$_SESSION['id'] > 0) {
  header('Location:' . $home_url . 'auth/login.php');
}else {
  if (isset($_SESSION['auth'])) {
    $url = (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
    if(strpos($url,'yoadmin') !== false){
      if ($_SESSION['admin']) {
      }else {
        header('Location:' . $home_url);
      }
    }
  }else {
    $_POST = array();
    $_SESSION['auth'] = 1;
    header('Location:' . $home_url);
  }
}

?>
