<?php include 'header.php' ?>

<div class="admin-wrap">

  <div class="sidebar-wrap"><?php include 'sidebar.php' ?></div>

  <div class="content-wrap">
    <?php
    include 'parts/task-list/task-list.php';
    ?>
  </div>

</div>
<?php include 'footer.php' ?>
