<?php include 'form_db.php' ?>

<script src="https://code.jquery.com/jquery-3.5.1.min.js"></script>

<div class="process-add-form part">
  <h3>ワークフローの追加・編集</h3>

  <div class="process-add-form__wrap">
    <form method="post" action="<?= home_url() . "yoadmin/process/insert" ?>" class="template">
      <input type="hidden" name="project_id" value="<?= $project_id ?>">
      <p class="text-r button-wrap"><span class="button blue add">追加</span></p>
      <div class="item-wrap">
        <ul class="sortable">
          <?php foreach ($project_items as $index => $item): ?>
            <li class="<?= $item['id'] ?>">
              <input type="hidden" name="list[][id]" value="<?= $item['id'] ?>">
              <div class="title">
                <i class="fas fa-arrows-alt"></i>
                <input type="text" name="list[][name]" value="<?= $item['name'] ?>">
              </div>
              <div class="memo">
                <textarea name="list[][memo]" rows="4"><?= $item['memo'] ?></textarea>
              </div>
              <p class="text-r button-wrap">
                <span class="button red delet">削除</span>
              </p>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <p class="text-r button-wrap">
        <span class="button blue add">追加</span>
        <button type="submit" class="button">保存</button>
      </p>
    </form>
  </div>
</div>

<script>
$(function() {
  var data = `
  <li class="new">
  <input type="hidden" name="list[][id]" value="new">
  <div class="title">
  <i class="fas fa-arrows-alt"></i>
  <input type="text" name="list[][name]" placeholder="タイトルを入力">
  </div>
  <div class="memo">
  <textarea name="list[][memo]" rows="4" placeholder="メモが必要な場合は入力"></textarea>
  </div>
  <p class="text-r button-wrap">
  <span class="button red delet">削除</span>
  </p>
  </li>`;

  $('.add').click(function() {
    $('.sortable').append($(data));
  });
});

$(function() {
  $(document).on("click",".delet", function() {
    if(!confirm('本当に削除しますか？')){
      /* キャンセルの時の処理 */
      return false;
    }else{
      /*　OKの時の処理 */
      $(this).parent().parent().remove();
    }
  });
});
</script>
