<!-- jQuery -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<!-- jQuery UI -->
<script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/jquery-ui.min.js"></script>
<link rel="stylesheet" href="https://ajax.googleapis.com/ajax/libs/jqueryui/1.12.1/themes/smoothness/jquery-ui.css">
<?php include 'form_db.php' ?>
<div class="mytask-add-form part">
  <h3><?= $project_item->name ?></h3>

  <div class="mytask-add-form__wrap">
    <form method="post" action="<?= home_url() . "yoadmin/mytask/insert" ?>" class="template">
      <input type="hidden" name="process_id" value="<?= $process_id ?>">
      <input id="remove_ids" type="hidden" name="remove_id" value="set">
      <p class="text-r button-wrap"><span class="button blue add">追加</span></p>
      <div class="item-wrap">
        <ul class="sortable">
          <?php foreach ($mytasks as $index => $item): ?>
            <li class="<?= $item['id'] ?>">
              <input type="hidden" name="list[][id]" value="<?= $item['id'] ?>">
              <div class="title">
                <i class="fas fa-arrows-alt"></i>
                <input type="text" name="list[][name]" value="<?= $item['name'] ?>">
              </div>
              <div class="memo">
                <textarea name="list[][memo]" rows="4"><?= $item['memo'] ?></textarea>
              </div>
              <div class="hour">
                <label>工数(時)</label>
                <input type="number" name="list[][hour]" value="<?= $item['hour'] ?>">
              </div>
              <p class="text-r button-wrap">
                <span class="button red delet">削除</span>
              </p>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>
      <p class="text-r button-wrap">
        <span class="button blue add">追加</span>
        <button type="submit" class="button">保存</button>
      </p>
    </form>
  </div>
</div>

<script>
$(function() {
  if ($('.sortable').length) {
    $('.sortable').sortable({handle: ".fa-arrows-alt"});
  }
});

</script>

<script>
$(function() {
  var data = `
  <li class="new">
  <input type="hidden" name="list[][id]" value="new">
  <div class="title">
  <i class="fas fa-arrows-alt"></i>
  <input type="text" name="list[][name]" placeholder="タイトルを入力">
  </div>
  <div class="memo">
  <textarea name="list[][memo]" rows="4" placeholder="メモが必要な場合は入力"></textarea>
  </div>
  <div class="hour">
  <label>工数(時)</label>
  <input type="text" name="list[][hour]">
  </div>
  <p class="text-r button-wrap">
  <span class="button red delet">削除</span>
  </p>
  </li>`;

  $('.add').click(function() {
    $('.sortable').append($(data));
  });
});

$(function() {
  $(document).on("click",".delet", function() {
    if(!confirm('本当に削除しますか？')){
      /* キャンセルの時の処理 */
      return false;
    }else{
      /*　OKの時の処理 */
      var mytask_id = $(this).parent().parent().attr("class");
      if (mytask_id != "new") {
        var remove_val = $('#remove_ids').val();
        var remove_val = remove_val + "," + mytask_id;
        $('#remove_ids').val(remove_val);
        console.log(remove_val);
      }

      $(this).parent().parent().remove();
    }
  });
});
</script>
