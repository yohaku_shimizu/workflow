<?php
include 'client-list_db.php';
?>
<div class="client-list part">
  <h3>顧客一覧</h3>
  <div class="client-list__table">
    <table class="list-table basic-table">
      <thead>
        <tr class="headings">
          <th>ID</th>
          <th>名前</th>
          <th>電話番号</th>
          <th>メールアドレス</th>
          <th>作成日</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($clients as $client): ?>
          <tr>
            <td><?= $client->id ?></td>
            <td><?= $client->name ?></td>
            <td><?= $client->tel ?></td>
            <td><?= $client->email ?></td>
            <td><?= $client->created ?></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
