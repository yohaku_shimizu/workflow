<?php

$projects = ORM::for_table('projects')
->find_many();

$clients = ORM::for_table('users')
->where_null('admin')
->find_many();

$project_type = ORM::for_table('project_type')
->find_many();

$client_simple_ary = array();
foreach ($clients as $client) {
  $client_simple_ary[$client->id] = $client->name;
}

$project_type_simple_ary = array();
foreach ($project_type as $type) {
  $project_type_simple_ary[$type->id] = $type->name;
}

?>
