<?php

function home_url() {
  global $home_url;
  return $home_url;
}

function get_url() {
  $url = (empty($_SERVER['HTTPS']) ? 'http://' : 'https://') . $_SERVER['HTTP_HOST'] . $_SERVER['REQUEST_URI'];
  return $url;
}

function temp_admin() {
  $home_url = home_url();
  $home_url .= "admin/view";

  return $home_url;
}

$java_temp_admin = temp_admin();
$java_temp_admin = json_encode( $java_temp_admin , JSON_HEX_TAG | JSON_HEX_AMP | JSON_HEX_APOS | JSON_HEX_QUOT);

function get_user() {
  global $user_id;
  $user_data = ORM::for_table('users')->where('id', $user_id)->find_one();
  return $user_data;
}

function debug($data) {
  echo "<pre class=\"prettyprint\">";
  var_dump($data);
  echo "</pre>";
}

// 指定したキーに対応する値を基準に、配列をソートする
function sortByKey($key_name, $sort_order, $array) {
  foreach ($array as $key => $value) {
    $standard_key_array[$key] = $value[$key_name];
  }

  array_multisort($standard_key_array, $sort_order, $array);

  return $array;
}

?>
