
<?php
var_dump($_POST);

$user_id = intval($_POST['client']);
$type = intval($_POST['type']);

try {

  $error_flg = true;

  $record = ORM::for_table('projects')->create();
  $record->name = $_POST['name'];
  $record->user_id = $user_id;
  $record->project_type = $type;
  $record->created = date('Y-m-d');

  $record->save();

} catch (\Exception $e) {
  $error_flg = false;
  echo "error";
} finally {
  if ($error_flg) {
    echo "成功";
  }
}

global $home_url;
header('Location:' . $home_url . 'yoadmin/process/add/' . $record['id']);
?>
