<?php include 'header.php' ?>

<div class="admin-wrap">

  <div class="sidebar-wrap"><?php include 'sidebar.php' ?></div>

  <div class="content-wrap">
    <?php
    include 'parts/mytask-list/list.php';
    ?>
  </div>

</div>
<?php include 'footer.php' ?>
