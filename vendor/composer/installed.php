<?php return array (
  'root' => 
  array (
    'pretty_version' => '1.0.0+no-version-set',
    'version' => '1.0.0.0',
    'aliases' => 
    array (
    ),
    'reference' => NULL,
    'name' => '__root__',
  ),
  'versions' => 
  array (
    '__root__' => 
    array (
      'pretty_version' => '1.0.0+no-version-set',
      'version' => '1.0.0.0',
      'aliases' => 
      array (
      ),
      'reference' => NULL,
    ),
    'j4mie/idiorm' => 
    array (
      'pretty_version' => 'v1.5.7',
      'version' => '1.5.7.0',
      'aliases' => 
      array (
      ),
      'reference' => 'd23f97053ef5d0b988a02c6a71eb5c6118b2f5b4',
    ),
    'maximebf/debugbar' => 
    array (
      'pretty_version' => 'v1.16.4',
      'version' => '1.16.4.0',
      'aliases' => 
      array (
      ),
      'reference' => 'c86c717e4bf3c6d98422da5c38bfa7b0f494b04c',
    ),
    'nikic/fast-route' => 
    array (
      'pretty_version' => 'v0.4.0',
      'version' => '0.4.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'f26a8f7788f25c0e3e9b1579d38d7ccab2755320',
    ),
    'psr/log' => 
    array (
      'pretty_version' => '1.1.3',
      'version' => '1.1.3.0',
      'aliases' => 
      array (
      ),
      'reference' => '0f73288fd15629204f9d42b7055f72dacbe811fc',
    ),
    'symfony/polyfill-mbstring' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '39d483bdf39be819deabf04ec872eb0b2410b531',
    ),
    'symfony/polyfill-php80' => 
    array (
      'pretty_version' => 'v1.20.0',
      'version' => '1.20.0.0',
      'aliases' => 
      array (
      ),
      'reference' => 'e70aa8b064c5b72d3df2abd5ab1e90464ad009de',
    ),
    'symfony/var-dumper' => 
    array (
      'pretty_version' => 'v5.2.1',
      'version' => '5.2.1.0',
      'aliases' => 
      array (
      ),
      'reference' => '13e7e882eaa55863faa7c4ad7c60f12f1a8b5089',
    ),
    'verot/class.upload.php' => 
    array (
      'pretty_version' => '2.1.0',
      'version' => '2.1.0.0',
      'aliases' => 
      array (
      ),
      'reference' => '4d9aae875245948d21e42ade332fb45f897f28bb',
    ),
  ),
);
