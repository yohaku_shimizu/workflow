<?php include 'db.php' ?>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>

<div class="report-add-form part">

  <h3>日報記入</h3>

  <div class="form-wrap">
    <form class="add-report template">
      <div class="form-parts">

        <select id="client-select">
          <option hidden>顧客を選択してください</option>
          <?php foreach ($clients as $client): ?>
            <option value="<?= $client->id ?>"><?= $client->name ?></option>
          <?php endforeach; ?>
        </select>
      </div>
      <div class="form-parts">
        <input id="task-name" name="task-name" value="タスクを選択" readonly>
        <input id="task-id" name="task-id" hidden value="">
      </div>
    </form>

    <div class="workflows">

    </div>
  </div>
</div>


<script>
$(function() {

  $('#client-select').change(function() {

    var post_url = location.protocol + "//" + location.host + "/workflowapi/get-workflow.php";
    var client_id = $(this).val();

    console.log(post_url);

    const postData = new FormData; // フォーム方式で送る場合
    postData.set('data', client_id); // set()で格納する

    const data = {
      method: 'POST',
      body: postData
    };

    fetch(post_url, data)
    .then((res) => {
      if (!res.ok) {
        throw new Error(`${res.status} ${res.statusText}`);
      }
      // 結果をjsonで取得
      return res.json();
    })
    .then((json) => {
      $('.workflows').empty();
      console.log(json);
      // json にデータが入る

      $.each(json, function(index, value) {
        var insert_dom = '';
        insert_dom += '<div data-projectId="' + value['id'] + '" class="project"><p>' + value['name'] + '</p><ul class="project-item">';

        $.each(value['project_items'], function(index, value) {

          insert_dom += '<li class="item-wrap"><p class="name">' + value['name'] + '</p><ul class="task">';

          $.each(value['mytasks'], function(index, value) {
            insert_dom += '<li data-taskId="' + value['id'] + '" >' + value['name'] + '</li>';
          })

          insert_dom += '</ul></li>';

        })
        insert_dom += '</ul></div>';
        $('.workflows').append(insert_dom).trigger("create");
      })

    })
    .catch((reason) => {
      console.log(reason);
    });

  })

  $(document).on('click', 'ul.task li', function() {
    $('ul.task li').removeClass('active');
    $(this).addClass('active');
    $('#task-name').attr('value',$(this).text());
    $('#task-id').attr('value', $(this).attr('data-taskId'));
  })

});


</script>
