<?php include 'header.php' ?>

<div class="admin-wrap">

  <div class="sidebar-wrap"><?php include 'sidebar.php' ?></div>

  <div class="content-wrap">
    <?php
    include 'parts/client-list/client-list.php';
    ?>
  </div>

</div>
<?php include 'footer.php' ?>
