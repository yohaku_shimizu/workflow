<?php include 'list_db.php' ?>

<div class="mytask-list part">
  
  <?php if (isset($project['id'])): ?>
    <p class="text-r mb"><a href="<?= home_url() . "yoadmin/process/view/" . $project['id'] ?>"><button class="button">戻る</button></a></p>
    <?php else: ?>
      <p class="text-r mb"><a href="<?= $_SERVER['HTTP_REFERER'] ?>"><button class="button">戻る</button></a></p>
  <?php endif; ?>
  <h3><?= $project_item['name'] ?>のタスク</h3>
  <p class="text-r mb"><a href="<?= home_url() . "yoadmin/mytask/add/" . $process_id ?>"><button class="button">編集・追加</button></a></p>
  <ul class="mytask-list__task">
    <?php foreach ($mytasks as $index => $task): ?>
      <li>
        <div class="hed">
          <p class="name <?php if ($index == 1): ?>active<?php endif; ?>"><?= $task['name'] ?></p>
          <p>設定工数 : <?= $task['hour'] ?>時間</p>
        </div>
        <div class="content">
          <?= $task['memo']; ?>
        </div>
      </li>
    <?php endforeach; ?>
  </ul>
</div>
