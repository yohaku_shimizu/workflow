<?php include 'form_db.php' ?>
<div class="client-add-form part">
  <h3>新規顧客アカウントを作成</h3>

  <div class="client-add-form__wrap">
    <form method="post" action="<?= home_url() . "yoadmin/projectadd/insert" ?>" class="template">

      <div class="form-parts">
        <label>プロジェクト名</label>
        <input name="name" type="text">
      </div>

      <div class="form-parts">
        <label>顧客選択</label>
        <select name="client">
          <option hidden>顧客を選択してください</option>
          <?php foreach ($client_simple_ary as $key => $client): ?>
            <option value="<?= $key ?>"><?= $client ?></option>
          <?php endforeach; ?>
        </select>
      </div>

      <div class="form-parts">
        <label>プロジェクトタイプ選択</label>
        <select name="type">
          <option hidden>プロジェクトタイプを選択してください</option>
          <?php foreach ($project_type_simple_ary  as $key => $type): ?>
            <option value="<?= $key ?>"><?= $type ?></option>
          <?php endforeach; ?>
        </select>
      </div>

      <p class="text-r"><button class="button">追加</button></p>
    </form>
  </div>
</div>
