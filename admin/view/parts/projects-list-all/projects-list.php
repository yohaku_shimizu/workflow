<?php
include 'projects-list_db.php';
?>

<div class="projects-list part">
  <h3>進行中プロジェクト一覧</h3>
  <div class="projects-list__table">
    <table class="list-table basic-table">
      <thead>
        <tr class="headings">
          <th>プロジェクトID</th>
          <th>名前</th>
          <th>クライアント名</th>
          <th>プロジェクトタイプ</th>
          <th>作成日</th>
          <th>納品</th>
        </tr>
      </thead>
      <tbody>
        <?php foreach ($projects as $project): ?>
          <tr>
            <td><?= $project->id ?></td>
            <td><?= $project->name ?></td>
            <td><?= $client_simple_ary[$project->user_id] ?></td>
            <td><?= $project_type_simple_ary[$project->project_type] ?></td>
            <td><?= $project->created ?></td>
            <td class="check <?php if ($project->completed): ?>fin<?php endif; ?>"><i class="fas fa-check"></i></td>
          </tr>
        <?php endforeach; ?>
      </tbody>
    </table>
  </div>
</div>
