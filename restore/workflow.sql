-- phpMyAdmin SQL Dump
-- version 5.0.1
-- https://www.phpmyadmin.net/
--
-- ホスト: 127.0.0.1
-- 生成日時: 2021-01-13 01:16:18
-- サーバのバージョン： 10.4.11-MariaDB
-- PHP のバージョン: 7.4.2

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- データベース: `workflow`
--

-- --------------------------------------------------------

--
-- テーブルの構造 `client_task`
--

CREATE TABLE `client_task` (
  `id` int(11) NOT NULL,
  `project_item_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `memo` blob NOT NULL,
  `tags` varchar(256) NOT NULL,
  `upload_typy` int(11) NOT NULL,
  `upload_date` date DEFAULT NULL,
  `upload_path` varchar(2083) NOT NULL,
  `upload_text` text NOT NULL,
  `upload_permit` tinyint(1) DEFAULT NULL,
  `created` date NOT NULL,
  `completed` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `client_task`
--

INSERT INTO `client_task` (`id`, `project_item_id`, `name`, `memo`, `tags`, `upload_typy`, `upload_date`, `upload_path`, `upload_text`, `upload_permit`, `created`, `completed`) VALUES
(1, 1, 'ToDo test case : 1 【テキストの入力】', 0xe38388e38383e38397e3839ae383bce382b8e2978be2978be381aee69687e7aba0e38292e7b48de59381e38197e381a6e3818fe381a0e38195e38184e38082e69687e5ad97e381af3330e69687e5ad97e7a88be5baa6e381a7e5ae9ce38197e3818fe3818ae9a198e38184e887b4e38197e381bee38199e38082, 'all,top', 1, '2021-01-05', '', 'test', NULL, '2020-12-27', '2021-01-06'),
(2, 2, 'ToDo test case : 2 【ファイルのアップロード】', '', 'about', 2, NULL, '', '', NULL, '2020-12-27', NULL),
(3, 3, 'ToDo test case : 3 【承認】', '', 'news', 3, NULL, '', '', NULL, '2020-12-27', NULL),
(4, 1, 'test : 承認済みtask', '', 'comp', 1, '2021-01-05', '', 'test', 1, '2021-01-05', '2021-01-05'),
(5, 1, 'test : 文章の納品❷', 0x61626f7574e3839ae383bce382b8e381aee2978be2978be38193e38293e381a6e38293e381a4e381aee2978be2978be381aee69687e7aba0e38292e7b48de59381e38292e3818ae9a198e38184e38197e381bee38199e38082e69687e5ad97e381af323530e69687e5ad97e7a88be5baa6e381a7e5ae9ce38197e3818fe3818ae9a198e38184e887b4e38197e381bee38199e38082, 'about', 1, '2021-01-05', '', 'つれづれなるまゝに、日暮らし、硯にむかひて、心にうつりゆくよしなし事を、そこはかとなく書きつくれば、あやしうこそものぐるほしけれ。（Wikipediaより）つれづれなるまゝに、日暮らし、硯にむかひて、心にうつりゆくよしなし事を、そこはかとなく書きつくれば、あやしうこそものぐるほしけれ。（Wikipediaより）つれづれなるまゝに、日暮らし、硯にむかひて、心にうつりゆくよしなし事を、そこはかとなく書きつくれば、あやしうこそものぐるほしけれ。（Wikipediaより）つれづれなるまゝに、日暮らし、硯', NULL, '2021-01-05', '2021-01-06'),
(6, 1, '文章の納品❸', 0xe59c92e995b7e58588e7949fe381aee38182e38184e38195e381a4e38292e7b48de59381e38197e381a6e3818fe381a0e38195e38184e38082e69687e5ad97e695b0e381af343030e69687e5ad97e7a88be5baa6e381a7e5ae9ce38197e3818fe3818ae9a198e38184e887b4e38197e381bee38199e38082, 'about', 1, '2021-01-04', '', '親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れぬ。別段深い理由でもない。新築の二階から首を出していたら、同級生の一人が冗談に、いくら威張っても、そこから飛び降りる事は出来まい。弱虫やーい。と囃したからである。小使に負ぶさって帰って来た時、おやじが大きな眼をして二階ぐらいから飛び降りて腰を抜かす奴があるかと云ったから、この次は抜かさずに飛んで見せますと答えた。（青空文庫より）親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れぬ。別段深い理由でもない。新築の二階から首を出していたら、同級生の一人が冗談に、いくら威張っても、そこから飛び降りる事は出来まい。弱虫やーい。と囃した', NULL, '2021-01-05', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `my_task`
--

CREATE TABLE `my_task` (
  `id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `project_item_id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `memo` text NOT NULL,
  `tags` varchar(256) NOT NULL,
  `hour` int(10) UNSIGNED NOT NULL,
  `progress` tinyint(1) DEFAULT NULL,
  `created` date NOT NULL,
  `completed` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `my_task`
--

INSERT INTO `my_task` (`id`, `priority`, `project_item_id`, `name`, `memo`, `tags`, `hour`, `progress`, `created`, `completed`) VALUES
(1, 0, 1, 'ヒアリング', 'メモスペース', 'all', 4, 1, '2020-12-27', NULL),
(3, 0, 2, 'コンテンツ整理', '掲載項目の整理を行ってください', 'top', 2, NULL, '2021-01-09', NULL),
(4, 2, 2, '掲載項目機能など書き込み', '掲載項目の表示内容や機能をカンプデータに書き込んでください。', 'top', 4, NULL, '2021-01-09', NULL),
(5, 1, 2, '内容チェック', 'コーダーは掲載するものに技術的問題が無いかなど確認を行ってください。', 'top', 2, NULL, '2021-01-09', NULL),
(6, 0, 0, 'カンプデータ調整', '掲載内容などに問題が無ければお客様に確認して頂くため調整を行ってください。', '', 0, NULL, '2021-01-09', NULL);

-- --------------------------------------------------------

--
-- テーブルの構造 `projects`
--

CREATE TABLE `projects` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_type` int(11) NOT NULL,
  `completed` tinyint(1) DEFAULT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `projects`
--

INSERT INTO `projects` (`id`, `name`, `user_id`, `project_type`, `completed`, `created`) VALUES
(1, 'コーポレートサイト制作', 1, 1, NULL, '2020-12-27'),
(2, '名刺作成', 1, 1, NULL, '2021-01-07'),
(3, 'チラシ作成', 1, 1, 1, '2021-01-07'),
(8, 'HP作成', 9, 1, NULL, '2021-01-09');

-- --------------------------------------------------------

--
-- テーブルの構造 `project_item`
--

CREATE TABLE `project_item` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL,
  `project_id` int(11) NOT NULL,
  `priority` int(11) NOT NULL,
  `memo` text NOT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `project_item`
--

INSERT INTO `project_item` (`id`, `name`, `project_id`, `priority`, `memo`, `created`) VALUES
(1, 'ワイヤーフレーム作成(サイト設計図作成)', 1, 2, '恥の多い生涯を送って来ました。自分には、人間の生活というものが、見当つかないのです。自分は東北の田舎に生れましたので、汽車をはじめて見たのは、よほど大きくなってからでした。自分は停車場のブリッジを、上って、降りて、そうしてそれが線路をまたぎ越えるために造られたものだという事には全然気づかず、ただそれは停車場の構内を外国の遊戯場みたいに、複雑に楽しく、ハイカラにするためにのみ、設備せられてあるものだとばかり思っていました。しかも、かなり永い間そう思っていたのです。ブリッジの上ったり降りたりは、自分にはむしろ、ずいぶん垢抜けのした遊戯で、それは鉄道のサーヴィスの中でも、最も気のきいたサーヴィスの一つだと思っていたのですが、のちにそれはただ旅客が線路をまたぎ越えるための頗る実利的な階段に過ぎないのを発見して、にわかに興が覚めました。また、自分は子供の頃、絵本で地下鉄道というものを見て、これもやは', '2020-12-27'),
(2, 'ヒアリング/基本設計', 1, 0, '吾輩は猫である。名前はまだ無い。どこで生れたかとんと見当がつかぬ。何でも薄暗いじめじめした所でニャーニャー泣いていた事だけは記憶している。吾輩はここで始めて人間というものを見た。しかもあとで聞くとそれは書生という人間中で一番獰悪な種族であったそうだ。この書生というのは時々我々を捕えて煮て食うという話である。しかしその当時は何という考もなかったから別段恐しいとも思わなかった。ただ彼の掌に載せられてスーと持ち上げられた時何だかフワフワした感じがあったばかりである。掌の上で少し落ちついて書生の顔を見たのがいわゆる人間というものの見始であろう。この時妙なものだと思った感じが今でも残っている。第一毛をもって装飾されべきはずの顔がつるつるしてまるで薬缶だ。その後猫にもだいぶ逢ったがこんな片輪には一度も出会わした事がない。のみならず顔の真中があまりに突起している。そうしてその穴の中から時々ぷうぷうと煙を', '2020-12-27'),
(3, 'デザインカンプ作成(サイト完成予想図作成)', 1, 3, '木曾路はすべて山の中である。あるところは岨づたいに行く崖の道であり、あるところは数十間の深さに臨む木曾川の岸であり、あるところは山の尾をめぐる谷の入り口である。一筋の街道はこの深い森林地帯を貫いていた。東ざかいの桜沢から、西の十曲峠まで、木曾十一宿はこの街道に添うて、二十二里余にわたる長い谿谷の間に散在していた。道路の位置も幾たびか改まったもので、古道はいつのまにか深い山間に埋もれた。名高い桟も、蔦のかずらを頼みにしたような危い場処ではなくなって、徳川時代の末にはすでに渡ることのできる橋であった。新規に新規にとできた道はだんだん谷の下の方の位置へと降って来た。道の狭いところには、木を伐って並べ、藤づるでからめ、それで街道の狭いのを補った。長い間にこの木曾路に起こって来た変化は、いくらかずつでも嶮岨な山坂の多いところを歩きよくした。そのかわり、大雨ごとにやって来る河水の氾濫が旅行を困難にする', '2020-12-27'),
(4, 'ヒアリング', 1, 1, '日本国民は、正当に選挙された国会における代表者を通じて行動し、われらとわれらの子孫のために、諸国民との協和による成果と、わが国全土にわたつて自由のもたらす恵沢を確保し、政府の行為によつて再び戦争の惨禍が起ることのないやうにすることを決意し、ここに主権が国民に存することを宣言し、この憲法を確定する。そもそも国政は、国民の厳粛な信託によるものであつて、その権威は国民に由来し、その権力は国民の代表者がこれを行使し、その福利は国民がこれを享受する。これは人類普遍の原理であり、この憲法は、かかる原理に基くものである。われらは、これに反する一切の憲法、法令及び詔勅を排除する。日本国民は、恒久の平和を念願し、人間相互の関係を支配する崇高な理想を深く自覚するのであつて、平和を愛する諸国民の公正と信義に信頼して、われらの安全と生存を保持しようと決意した。われらは、平和を維持し、専制と隷従、圧迫と偏狭を地上か', '2021-01-08'),
(5, 'トップページビジュアル確認用ページ作成', 1, 4, '親譲りの無鉄砲で小供の時から損ばかりしている。小学校に居る時分学校の二階から飛び降りて一週間ほど腰を抜かした事がある。なぜそんな無闇をしたと聞く人があるかも知れぬ。別段深い理由でもない。新築の二階から首を出していたら、同級生の一人が冗談に、いくら威張っても、そこから飛び降りる事は出来まい。弱虫やーい。と囃したからである。小使に負ぶさって帰って来た時、おやじが大きな眼をして二階ぐらいから飛び降りて腰を抜かす奴があるかと云ったから、この次は抜かさずに飛んで見せますと答えた。（青空文庫より）', '2021-01-08'),
(6, 'test', 7, 0, 'test', '2021-01-09'),
(7, 'sss', 7, 1, 'das', '2021-01-09'),
(8, 'ヒアリング', 8, 0, 'ヒアリングメモ', '2021-01-09'),
(9, 'フレームワーク作成', 8, 1, 'めも', '2021-01-09'),
(10, 'カンプ作成', 8, 2, 'めも', '2021-01-09');

-- --------------------------------------------------------

--
-- テーブルの構造 `project_type`
--

CREATE TABLE `project_type` (
  `id` int(11) NOT NULL,
  `name` varchar(256) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `project_type`
--

INSERT INTO `project_type` (`id`, `name`) VALUES
(1, 'コーポレートサイト');

-- --------------------------------------------------------

--
-- テーブルの構造 `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `client_number` int(11) NOT NULL,
  `login_id` varchar(256) NOT NULL,
  `login_password` varchar(256) NOT NULL,
  `name` varchar(256) NOT NULL,
  `tel` varchar(256) NOT NULL,
  `email` varchar(256) NOT NULL,
  `admin` tinyint(1) DEFAULT NULL,
  `created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- テーブルのデータのダンプ `users`
--

INSERT INTO `users` (`id`, `client_number`, `login_id`, `login_password`, `name`, `tel`, `email`, `admin`, `created`) VALUES
(1, 0, 'ylc', '$2y$10$Zi3QzjejJ0a4qTPwj7TELONl4LF3FrVS2ahJQIC/XA7Ym9b4twh/W', 'ワイエルシー', '09084598999', 'shimizu.s@yohaku-bunka.com', NULL, '2020-12-27'),
(4, 12, 'shimizu', '$2y$10$Zi3QzjejJ0a4qTPwj7TELONl4LF3FrVS2ahJQIC/XA7Ym9b4twh/W', '清水創太', '09084598999', 'shimizu.s@yohaku-bunka.com', 1, '2021-01-06'),
(8, 123, 'yac', '$2y$10$oi4XmjSK1UvexjgZ9Qi.sOEOJqXEsdRq3P2Dj6.NNq1QFBi/q/Q8i', '槌屋ヤック', '09084598999', 'shimizu.s@yohaku-bunka.com', NULL, '2021-01-09'),
(9, 1234, 'yohaku', '$2y$10$akvPauGTnIzvSgtQ/o4o7uEJWqlDXnwCISxySFwZoDgOtg7BAVaha', '余白文化', '09084598999', 'shimizu.s@yohaku-bunka.com', NULL, '2021-01-09');

--
-- ダンプしたテーブルのインデックス
--

--
-- テーブルのインデックス `client_task`
--
ALTER TABLE `client_task`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `my_task`
--
ALTER TABLE `my_task`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `projects`
--
ALTER TABLE `projects`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `project_item`
--
ALTER TABLE `project_item`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `project_type`
--
ALTER TABLE `project_type`
  ADD PRIMARY KEY (`id`);

--
-- テーブルのインデックス `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- ダンプしたテーブルのAUTO_INCREMENT
--

--
-- テーブルのAUTO_INCREMENT `client_task`
--
ALTER TABLE `client_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=7;

--
-- テーブルのAUTO_INCREMENT `my_task`
--
ALTER TABLE `my_task`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- テーブルのAUTO_INCREMENT `projects`
--
ALTER TABLE `projects`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- テーブルのAUTO_INCREMENT `project_item`
--
ALTER TABLE `project_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- テーブルのAUTO_INCREMENT `project_type`
--
ALTER TABLE `project_type`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- テーブルのAUTO_INCREMENT `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
