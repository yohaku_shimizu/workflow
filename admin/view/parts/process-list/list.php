<?php include 'list_db.php' ?>

<div class="process-list part">
  <h3><?= $project->name ?> の開発工程</h3>
  <p class="text-r mb"><a href="<?= home_url() . "yoadmin/process/add/" . $project_id ?>"><button class="button">編集・追加</button></a></p>
  <ul class="process-list__process">
    <?php
    $active_flug = false;
    $active_onec_flug = false;
    ?>
    <?php foreach ($process as $index => $solo): ?>
      <?php
      $my_tasks = ORM::for_table('my_task')
      ->where('project_item_id',$solo->id)
      ->find_many();
      $process_hour = 0;
      $comp_flug = true;
      foreach ($my_tasks as $key => $task) {
        $process_hour += $task->hour;
        if ($task->completed == NULL) {
          $comp_flug = false;
          if ($active_onec_flug == false) {
            $active_flug = true;
          }
        }
      }
      ?>
      <li>
        <div class="hed">
          <p class="name <?php
          if ($active_flug) {
            echo "active";
            $active_flug = false;
            $active_onec_flug = true;
          }
          ?>">
          <?= $solo['name'] ?>
          <span class="title-item-wrap">
            <span><?php if ($comp_flug): ?>✅<?php else : ?>❌<?php endif; ?></span>
            <span><i class="far fa-clock"></i> : <?= $process_hour ?>時間</span>
            <span><a class="subcolor" href="<?= home_url() ?>yoadmin/mytask/add/<?= $solo['id'] ?>">余白タスク編集</a></span>
            <span><a class="subcolor" href="<?= home_url() ?>yoadmin/task/add/<?= $solo['id'] ?>">クライアントタスク追加</a></span>
          </span>
        </p>

        <p class="memo"><?= $solo['memo'] ?></p>

        <ul class="mytask-list__task">
          <?php foreach ($my_tasks as $index => $task): ?>
            <li>
              <div class="hed">
                <p class="name <?php if ($index == 1): ?>active<?php endif; ?>"><?= $task['name'] ?></p>
                <p><i class="far fa-clock"></i> : <?= $task['hour'] ?>時間</p>
              </div>
              <div class="content">
                <?= $task['memo']; ?>
              </div>
            </li>
          <?php endforeach; ?>
        </ul>

        </div>
      </li>
    <?php endforeach; ?>
  </ul>
</div>
