<div class="sidebar">

  <div class="sidebar__user">
    <div class="user-image">
      <p><img src="https://placehold.jp/150x150.png" /></p>
    </div>
    <p class="user-name"><?= $user['name'] ?></p>
  </div>

  <div class="sidebar__todo">
    <?php foreach ($client_task as $task): ?>
      <?php if (!$task['completed']): ?>
        <p>未処理のToDoがあります。</p>
        <a href="#"><?= $task['name'] ?></a>
      <?php endif; ?>
    <?php endforeach; ?>
  </div>

  <div class="sidebar__links">
    <ul>
      <li><a href="#">プロジェクト一覧</a></li>
      <li><a href="#">ご利用ガイド</a></li>
      <li><a href="#">アカウント詳細</a></li>
      <li><a href="#">お問い合わせ</a></li>
    </ul>
  </div>

</div>
