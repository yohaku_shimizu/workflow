<script src="http://code.jquery.com/jquery-1.8.3.min.js"></script>
<script src="http://code.jquery.com/ui/1.11.3/jquery-ui.js"></script>

<?php
$inc_file = basename(__FILE__, ".php") . "_db.php";
include $inc_file;

global $vars;
if (isset($vars['id'])) {
  $task_id = $vars['id'];
  $record = ORM::for_table('client_task')->where('id',$task_id)->find_one();
  $record->completed = date('Y-m-d');
  $record->save();
  header('Location:' . home_url() . 'yoadmin');
}
?>
<div class="tasks part">
  <h3>未承認のタスクリスト</h3>

  <div class="wrap">
    <?php if ($tasks): ?>
      <div class="tasks__list">
        <ul>
          <?php foreach ($tasks as $index => $task): ?>
            <li class="tasks__list--onece">
              <div class="block <?php if ($index == 0): ?>active<?php endif; ?>">
                <p class="title"><?= $task['name'] ?></p>
                <p class="date">
                  <?php
                  $now = strtotime(date('Y-m-d'));
                  $task_date = strtotime($task['upload_date']);
                  $diffe_date = ($now - $task_date) / (60 * 60 * 24);
                  if ($diffe_date == 0) {
                    $diffe_date = "本日提出されました";
                    echo $diffe_date;
                  }else {
                    $diffe_date .= "日前に提出されました";
                    echo $diffe_date;
                  }
                  ?>
                </p>
                <div class="memo">
                  <?= $task['memo'] ?>
                </div>
              </div>
            </li>
          <?php endforeach; ?>
        </ul>
      </div>

      <?php foreach ($tasks as $index => $task): ?>
        <div class="tasks__data index <?php if ($index == 0): ?>active<?php endif; ?>">

          <div class="anser"><?= $task['upload_text'] ?></div>
            <p class="text-r"><a href="<?= home_url() . "yoadmin/" . $task['id'] ?>"><button type="submit" class="button submit">承認</button></a></p>
        </div>
      <?php endforeach; ?>
      <?php else: ?>
        <p class="not-task">未承認のタスクはありません。</p>
    <?php endif; ?>

  </div>

</div>

<script>
$(function() {
  $('.tasks__list--onece').on('click',function(){
    var index = $('.tasks__list--onece').index(this);
    console.log(index);
    $('.tasks__list--onece .block').removeClass('active');
    $(this).find('.block').addClass('active');
    $('.tasks__data').removeClass('active');
    $('.tasks__data').eq(index).addClass('active');
  });
  $('.submit').on('click',function(){
    if(!confirm('本当に承認しますか？')){
        /* キャンセルの時の処理 */
        return false;
    }else{
        /*　OKの時の処理 */
    }
  });
});
</script>
