<?php
include 'list_db.php';
?>

<div class="task-all part">
  <table class="basic-table">
    <thead>
      <tr class="headings">
        <td>顧客名</td>
        <td>タスク名</td>
        <td>作成日</td>
        <td>ステータス</td>
        <td></td>
      </tr>
    </thead>
    <tbody>
      <?php foreach ($all_task as $index => $task): ?>
        <tr>
          <td><?= client_serch($task->project_item_id) ?></td>
          <td><?= $task->name ?></td>
          <td><?= $task->created ?></td>
          <td>
            <?php
            if ($task->upload_date) {
              if ($task->completed) {
                echo '<span class="subcolor">提出済み<span>';
              }else {
                echo '<span class="yellow">確認中<span>';
              }
            }else {
              echo '<span class="red">未提出<span>';
            }
            ?>
          </td>
          <td><a href="<?= home_url() ?>yoadmin/task/edit/<?= $task->id ?>">編集</a></td>
        </tr>
      <?php endforeach; ?>
    </tbody>
  </table>
</div>
