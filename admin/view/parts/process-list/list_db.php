<?php

$project_id = intval($vars['id']);

$project = ORM::for_table('projects')
->where('id',$project_id)
->find_one();

$process_id = intval($vars['id']);
$process = ORM::for_table('project_item')
->where('project_id',$process_id)
->find_many();


if ($process) {
  $process = sortByKey('priority', SORT_ASC, $process);
}
?>
