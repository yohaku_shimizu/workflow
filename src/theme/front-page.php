<?php include 'header.php' ?>

<main class="basic-wrap">
  <!-- <div class="sidebar-wrap">
  <?php include 'sidebar.php' ?>
</div> -->

<div class="content">

  <div class="content__header">
    <h2>
      ようこそ、<span><?= $user['name'] ?></span>さん
    </h2>
    <p>
      反映にはお時間がかかる場合がございます。<br />2～3営業日を過ぎても反映されていない場合は、お手数ですがご連絡ください。
    </p>
  </div>

  <div class="content__projects">
    <h3>プロジェクト一覧</h3>
    <?php
    foreach ((array) $projects as $key => $value) {
      $sort[$key] = $value['completed'];
    }
    // debug($client_task);
    array_multisort($sort, SORT_ASC, $projects);
    ?>

    <div class="content__projects--status-intro flex">
      <p class="progress"><i class="fas fa-tag"></i>進行中</p>
      <p class="complete"><i class="fas fa-tag"></i>納品完了</p>
      <p class="sort"><i class="fas fa-filter"></i>絞り込み</p>
    </div>
    <div class="content__projects--status-box flex">
      <?php foreach ($projects as $project): ?>
        <?php $task_flg = false;?>
          <div class="content__projects--status flex">

            <div class="doc">
              <!-- <p>プロジェクト番号<span><?= $project['id'] ?></span></p> -->
              <p><span><?= $project['name'] ?></span></p>
            </div>


            <div class="progress">
              <?php if ($project['completed']): ?>
                <p class="complete"><i class="fas fa-tag"></i>納品済み</p>
              <?php else: ?>
                <p class="notcomp"><i class="fas fa-tag"></i>進行中</p>
              <?php endif; ?>
            </div>

            <div class="untreated">
              <?php
                $ary = [];
                foreach ($project_item as $item) {
                  foreach ($item as $solo) {
                    if($solo['project_id'] === $project['id'] ){
                      array_push($ary,$solo['id']);
                    }
                  }
                }
                foreach ($client_task as $key => $value) {
                  foreach ($ary as $key => $num) {
                    if($value["project_item_id"] === $num){
                      if($value["upload_date"] === NULL){
                        $task_flg = true;
                      }
                    }
                  }
                }
              ?>
              <?php if ($task_flg): ?>
                <p><i class="fas fa-exclamation-circle"></i>未処理のタスクがあります</p>
              <?php endif; ?>

            </div>

            <div class="pick">
              <p>選択する<i class="fas fa-arrow-circle-right"></i></p>
            </div>
            <a href="<?= home_url() ?>task/<?= $project->id ?>"></a>
          </div>
      <?php endforeach; ?>
    </div>
  </div>

</div>

</main>

<?php include 'footer.php' ?>
