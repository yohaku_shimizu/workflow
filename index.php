<?php
require 'config/root.php'; // root 読み込み

require 'vendor/autoload.php'; // composer autoload 読み込み
require 'vendor/j4mie/idiorm/idiorm.php'; // idiorm 読み込み (PDO処理)

require 'config/database.php'; // DB情報 読み込み

// ログイン認証
require 'auth/auth.php';

// ルーティング設定
$handlers = function(FastRoute\RouteCollector $r) use($base) {
  // front
  $r->addRoute('GET', $base, 'index');
  $r->addRoute('GET', $base.'task/{id:\d+}', 'task');
  $r->addRoute('GET', $base.'user', 'user');
  $r->addRoute('GET', $base.'logout', 'logout');

  // admin
  // top *client task
  $r->addRoute('GET', $base.'yoadmin', 'yoadmin');
  $r->addRoute('GET', $base.'yoadmin/dashboard', 'yoadminDashboard');
  $r->addRoute('GET', $base.'yoadmin/{id:\d+}', 'yoadmin');
  // client
  $r->addRoute('GET', $base.'yoadmin/client', 'yoadminClient');
  $r->addRoute('GET', $base.'yoadmin/clientadd', 'yoadminClientAdd');
  $r->addRoute('POST', $base.'yoadmin/clientadd/insert', 'yoadminClientInset');
  // project
  $r->addRoute('GET', $base.'yoadmin/projects', 'yoadminProjects');
  $r->addRoute('GET', $base.'yoadmin/projectadd', 'yoadminProjectAdd');
  $r->addRoute('POST', $base.'yoadmin/projectadd/insert', 'yoadminProjectInset');
  $r->addRoute('GET', $base.'yoadmin/projects/all', 'yoadminProjectsAll');
  //process
  $r->addRoute('GET', $base.'yoadmin/process/add/{id:\d+}', 'yoadminProcessAdd');
  $r->addRoute('POST', $base.'yoadmin/process/insert', 'yoadminProcessInsert');
  $r->addRoute('GET', $base.'yoadmin/process/view/{id:\d+}', 'yoadminProcessView');
  //mytask
  $r->addRoute('GET', $base.'yoadmin/mytask/view/{id:\d+}', 'yoadminMytaskView');
  $r->addRoute('GET', $base.'yoadmin/mytask/add/{id:\d+}', 'yoadminMytaskAdd');
  $r->addRoute('POST', $base.'yoadmin/mytask/insert', 'yoadminMytaskInsert');
  //crilent task
  $r->addRoute('GET', $base.'yoadmin/task/add/{id:\d+}', 'yoadminTaskAdd');
  $r->addRoute('POST', $base.'yoadmin/task/add/insert', 'yoadminTaskInsert');
  $r->addRoute('GET', $base.'yoadmin/task/all', 'yoadminTaskAll');
  $r->addRoute('GET', $base.'yoadmin/task/edit/{id:\d+}', 'yoadminTaskEdit');
  $r->addRoute('POST', $base.'yoadmin/task/edit/insert', 'yoadminTaskEditInsert');
  //report
  $r->addRoute('GET', $base.'yoadmin/report/add', 'yoadminReportAdd');
  $r->addRoute('POST', $base.'yoadmin/report/get', 'yoadminReportGetWorkflow');
};


// $dispatcher = FastRoute\simpleDispatcher($handlers);

$dispatcher = FastRoute\cachedDispatcher($handlers, [
  'cacheFile' => __DIR__ . '/route.cache',
  'cacheDisabled' => true
]);

$uri = $_SERVER['REQUEST_URI'];
$method = $_SERVER['REQUEST_METHOD'];
$routeInfo = $dispatcher->dispatch($method, $uri);
switch ($routeInfo[0]) {
  case FastRoute\Dispatcher::NOT_FOUND:
  echo "ページが見つかりませんでした。\n";
  break;
  case FastRoute\Dispatcher::METHOD_NOT_ALLOWED:
  $allowedMethods = $routeInfo[1];
  echo "許可されない HTTP リクエストです。\n";
  break;
  case FastRoute\Dispatcher::FOUND:
  $handler = $routeInfo[1];
  $vars = $routeInfo[2];

  // choose a template for front
  if ($handler) {
    $readTemplate = "src/theme/";
    if ($handler == "index") {
      $readTemplate .= "front-page.php";
    }else {
      $readTemplate .= "page-" . $handler . ".php";
    }
    if (!file_exists($readTemplate)) {
      $readTemplate = "src/theme/index.php";
    }
  }

  // choose a template for admin
  if ($handler == "yoadmin") {
    $readTemplate = "admin/view/index.php";
  }
  if ($handler == "yoadminClient") {
    $readTemplate = "admin/view/client.php";
  }
  if ($handler == "yoadminClientAdd") {
    $readTemplate = "admin/view/client-add.php";
  }
  if ($handler == "yoadminClientInset") {
    $readTemplate = "admin/view/parts/client-add/insert.php";
  }
  if ($handler == "yoadminProjects") {
    $readTemplate = "admin/view/projects.php";
  }
  if ($handler == "yoadminProjectAdd") {
    $readTemplate = "admin/view/project-add.php";
  }
  if ($handler == "yoadminProjectInset") {
    $readTemplate = "admin/view/parts/project-add/insert.php";
  }
  if ($handler == "yoadminProjectsAll") {
    $readTemplate = "admin/view/projects-all.php";
  }
  if ($handler == "yoadminProcessAdd") {
    $readTemplate = "admin/view/process-add.php";
  }
  if ($handler == "yoadminProcessInsert") {
    $readTemplate = "admin/view/parts/process-add/insert.php";
  }
  if ($handler == "yoadminProcessView") {
    $readTemplate = "admin/view/process-list.php";
  }
  if ($handler == "yoadminMytaskView") {
    $readTemplate = "admin/view/mytask-list.php";
  }
  if ($handler == "yoadminMytaskAdd") {
    $readTemplate = "admin/view/mytask-add.php";
  }
  if ($handler == "yoadminMytaskInsert") {
    $readTemplate = "admin/view/parts/mytask-add/insert.php";
  }
  if ($handler == "yoadminTaskAdd") {
    $readTemplate = "admin/view/task-add.php";
  }
  if ($handler == "yoadminTaskInsert") {
    $readTemplate = "admin/view/parts/task-add/insert.php";
  }
  if ($handler == "yoadminTaskAll") {
    $readTemplate = "admin/view/task-all.php";
  }
  if ($handler == "yoadminDashboard") {
    $readTemplate = "admin/view/dashboard.php";
  }
  if ($handler == "yoadminTaskEdit") {
    $readTemplate = "admin/view/task-edit.php";
  }
  if ($handler == "yoadminTaskEditInsert") {
    $readTemplate = "admin/view/parts/task-edit/insert.php";
  }
  if ($handler == "yoadminReportAdd") {
    $readTemplate = "admin/view/report-add.php";
  }
  if ($handler == "yoadminReportGetWorkflow") {
    $readTemplate = "admin/view/parts/report-add/get-workflow.php";
  }

  echo $handler($vars,$readTemplate);
  break;
}


// 使用関数

// front
function index($vars,$readTemplate = null)
{
  include $readTemplate;
}

function task($vars,$readTemplate = null)
{
  include $readTemplate;
}

function user($vars)
{
  return "userに関するページです。\n";
}

function logout($vars)
{
  require 'auth/logout.php';
}


// admin
function yoadmin($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminClient($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminClientAdd($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminClientInset($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminProjects($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminProjectAdd($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminProjectInset($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminProjectsAll($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminProcessAdd($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminProcessInsert($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminProcessView($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminMytaskView($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminMytaskAdd($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminMytaskInsert($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminTaskAdd($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminTaskInsert($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminTaskAll($vars,$readTemplate = null)
{
  include $readTemplate;
}
function yoadminDashboard($vars,$readTemplate = null)
{
  include $readTemplate;
}

function yoadminTaskEdit($vars,$readTemplate = null)
{
  include $readTemplate;
}

function yoadminTaskEditInsert($vars,$readTemplate = null)
{
  include $readTemplate;
}

function yoadminReportAdd($vars,$readTemplate = null)
{
  include $readTemplate;
}

function yoadminReportGetWorkflow($vars,$readTemplate = null)
{
  include $readTemplate;
}

?>
