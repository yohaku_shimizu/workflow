<?php

$user = get_user();

$projects = ORM::for_table('projects')->where('user_id', $user['id'])->find_many();

$client_task = array();
$my_task = array();
$project_item = array();
foreach ($projects as $key => $project) {
  $project_item[$key] = ORM::for_table('project_item')->where('project_id', $project['id'])->find_many();
  foreach ($project_item[$key] as $item) {
    $client_task = array_merge($client_task,ORM::for_table('client_task')->where('project_item_id', $item['id'])->find_many());
    $my_task = array_merge($my_task,ORM::for_table('my_task')->where('project_item_id', $item['id'])->find_many());
  }
}

//debug($client_task);
//debug($my_task);

function get_project_task($id,$project_item,$client_task){
  $ary = [];
  $return_array = [];
  $id = intval($id);

  foreach ($project_item as $item) {
    foreach ($item as $solo) {
      if($solo['project_id'] === $id ){
        array_push($ary,$solo['id']);
      }
    }
  }
  foreach ($client_task as $key => $value) {
    foreach ($ary as $key => $num) {
      if($value["project_item_id"] === $num){
        array_push($return_array,$value);
      }
    }
  }
  return $return_array;
}

?>
